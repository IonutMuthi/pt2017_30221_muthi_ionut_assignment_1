import static org.junit.Assert.*;

public class Test {
 Polinom p1=new Polinom(null);
 Polinom p2=new Polinom(null);
 Polinom prez=new Polinom(null);
 Polinom pcorect=new Polinom(null);
 Operatii o = new Operatii();
	@org.junit.Test
	public void testadd() {
		p1 = p1.citire("+3x^2");
		p2=p2.citire("+4x^2");	
		prez = o.addPol(p1, p2);
		pcorect =pcorect.citire("+7x^2");
		assertEquals(prez, pcorect);
	}
	
	@org.junit.Test
	public void testsub() {
		p1 = p1.citire("+3x^2");
		p2=p2.citire("+4x^2");	
		prez = o.subPol(p1, p2);
		pcorect =pcorect.citire("-1x^2");
		assertEquals(prez, pcorect);
	}
	
	@org.junit.Test
	public void testderivare() {
		p1 = p1.citire("+3x^2");
		prez = o.derivare(p1);
		pcorect =pcorect.citire("+6x^1");
		assertEquals(prez, pcorect);
	}
	
	@org.junit.Test
	public void testintegrare() {
		p1 = p1.citire("+3x^2");
		prez = o.integreaza(p1);
		pcorect =pcorect.citire("+1x^3");
		assertEquals(prez, pcorect);
	}

	@org.junit.Test
	public void testmul() {
		p1 = p1.citire("+3x^2");
		p2=p2.citire("+4x^2");	
		prez = o.mulPol(p1, p2);
		pcorect =pcorect.citire("+12x^4");
		assertEquals(prez, pcorect);
	}

}
