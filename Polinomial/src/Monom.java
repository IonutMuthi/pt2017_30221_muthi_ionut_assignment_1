
public class Monom {
	
	public double coeficient;
	public int grad;
	
	public Monom( double coeficient, int grad){
		this.grad=grad;
		this.coeficient=coeficient;
	}

	public int getGrad() {
		return grad;
	}
	public void setGrad(int grad) {
		this.grad = grad;
	}
	public double getCoeficient() {
		return coeficient;
	}
	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}
	
	
	public Monom impMonom(Monom m1, Monom m2){
		Monom m3=new Monom((m1.getCoeficient()/m2.getCoeficient()),(m1.getGrad()-m2.getGrad()));
		return m3;
	}
	
	public boolean isInt(double coeficient){
		String str=""+coeficient;
		if(str.substring(0, str.length()).contains(".0"))
				return true;
		else 
			return false;
	}
	
	@Override
	public String toString(){
		if(isInt(coeficient)){
			if(coeficient !=0){
				if(grad == 0){
					if(coeficient>0)
						return "+ " +(int)coeficient;
					else
						return "- " +(int)coeficient*(-1);
				}
				else if(grad==1){
						if(coeficient>0)
							return "+ " +(int)coeficient+"x";
						else
							return "- " +(int)coeficient*(-1)+"x";			
				}
				else if(coeficient>0)
					return "+ " +(int)coeficient+"x^"+grad;
				else
					return "- " +(int)coeficient*(-1)+"x^"+grad;
			}
			else 
				return "";
		}
		else if(coeficient !=0){
			if(grad==0){
				if(coeficient>0)
					return "+ " + coeficient;
				else
					return "- " + coeficient*(-1);
			}
			else if(grad==1){
				if(coeficient>0)
					return "+ " + coeficient +"x";
				else
					return "- " + coeficient*(-1) + "x";
			}
			else if(coeficient>0)
				return "+ " + String.format("%.1f", coeficient)+ "x^"+grad;
			else
				return "- "+ String.format("%.1f",coeficient)+ "x^"+grad;
		}
		else return "";
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Monom other = (Monom) obj;
		if (Double.doubleToLongBits(coeficient) != Double.doubleToLongBits(other.coeficient))
			return false;
		if (grad != other.grad)
			return false;
		return true;
	}
	
	
}
