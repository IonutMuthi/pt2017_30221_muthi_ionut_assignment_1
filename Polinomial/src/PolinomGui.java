import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class PolinomGui extends JFrame {
	private JPanel mainPanel=new JPanel();
	
	// labels
	private JLabel texLabel=new JLabel("Polinoamele introduse trebuie sa fie de forma : +ax^b+cx^d");
	private JLabel p1Label=new JLabel("P1(x)= ");
	private JLabel p2Label=new JLabel("P2(x)= ");
	private JLabel rezLabel=new JLabel("Rezultat= ");
	private JLabel restLabel=new JLabel("Rest= ");
	
	//text
	private JTextField p1Text=new JTextField(" ");
	private JTextField p2Text=new JTextField(" ");
	private JTextField rezText=new JTextField(" ");
	private JTextField restText=new JTextField(" ");
	
	//butoane
	private JButton addBtn=new JButton("+");
	private JButton subBtn=new JButton("-");
	private JButton mulBtn=new JButton("*");
	private JButton divBtn=new JButton("/");
	private JButton intp1Btn=new JButton("Inegreaza P1(x)");
	private JButton derp1Btn=new JButton("Deriveaza P1(x)");
	private JButton intp2Btn=new JButton("Inegreaza P2(x)");
	private JButton derp2Btn=new JButton("Deriveaza P2(x)");

	//polinoamele
	Polinom p1=new Polinom(null);
	Polinom p2=new Polinom(null);
	
	Operatii o=new Operatii();
	
	public PolinomGui(){
		interfaceSetings();
		initComponents();
		addComponents();
		addListeners();
	}
	private void interfaceSetings(){
		this.setLocation(650,250);
		this.setSize(540,370);
		this.setTitle("Polynomials Operations");		
		this.setVisible(true);
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.add(mainPanel);
	}
	
	private void initComponents(){
		mainPanel.setLayout(null);
		texLabel.setBounds(130, 15, 350, 35);
		p1Label.setBounds(50, 50, 100, 35);
		p2Label.setBounds(50, 100, 100, 35);
		rezLabel.setBounds(43, 267, 100, 35);
		restLabel.setBounds(50, 305, 100, 35);
		restLabel.setVisible(false);
		
		texLabel.setHorizontalAlignment(SwingConstants.CENTER);
		p1Label.setHorizontalAlignment(SwingConstants.RIGHT);
		p2Label.setHorizontalAlignment(SwingConstants.RIGHT);
		rezLabel.setHorizontalAlignment(SwingConstants.LEFT);
		restLabel.setHorizontalAlignment(SwingConstants.LEFT);
		
		p1Text.setBounds(150, 50, 325, 35);
		p2Text.setBounds(150, 100, 325, 35);
		rezText.setBounds(95, 265, 380, 35);
		restText.setBounds(95, 300, 380, 35);
		restText.setVisible(false);
		
		p1Text.setHorizontalAlignment(SwingConstants.CENTER);
		p2Text.setHorizontalAlignment(SwingConstants.CENTER);
		rezText.setHorizontalAlignment(SwingConstants.CENTER);
		restText.setHorizontalAlignment(SwingConstants.CENTER);
		
		addBtn.setBounds(90, 150, 80, 35);
		subBtn.setBounds(172, 150, 80, 35);
		mulBtn.setBounds(273, 150, 80, 35);
		divBtn.setBounds(353, 150, 80, 35);
		derp1Btn.setBounds(110, 186, 125, 35);
		derp2Btn.setBounds(110, 220, 125, 35);
		intp1Btn.setBounds(290, 186, 125, 35);
		intp2Btn.setBounds(290, 220, 125, 35);	
	}
	
	private void addComponents(){
	      // adaugam componentele in panel
		mainPanel.add(texLabel);
		mainPanel.add(p1Label);
		mainPanel.add(p2Label);
		mainPanel.add(rezLabel);
		mainPanel.add(restLabel);

		mainPanel.add(p1Text);
		mainPanel.add(p2Text);
		mainPanel.add(rezText);
		mainPanel.add(restText);
		
		mainPanel.add(addBtn);
		mainPanel.add(subBtn);
		mainPanel.add(mulBtn);
		mainPanel.add(divBtn);
		mainPanel.add(derp1Btn);
		mainPanel.add(derp2Btn);
		mainPanel.add(intp1Btn);
		mainPanel.add(intp2Btn);
	}
	
	private Polinom readPol(String string){
		Polinom newPol = new Polinom(null).citire(string);
		System.out.println(newPol.toString());
		restLabel.setVisible(false);
		restText.setVisible(false);
		return newPol;
	}
	
	private void addListeners(){
		
		addBtn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p1 = readPol(p1Text.getText());
				p2 = readPol(p2Text.getText());
				rezText.setText(o.addPol(p1,p2).toString());
			}	
		});
		
		subBtn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p1 = readPol(p1Text.getText());
				p2 = readPol(p2Text.getText());
				rezText.setText(o.subPol(p1,p2).toString());
			}	
		});
		mulBtn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p1 = readPol(p1Text.getText());
				p2 = readPol(p2Text.getText());
				rezText.setText(o.mulPol(p1,p2).toString());
			}	
		});
		
		divBtn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p1 = readPol(p1Text.getText());
				p2 = readPol(p2Text.getText());
				restLabel.setVisible(true);
				restText.setVisible(true);	
				ArrayList<Polinom> rez=new ArrayList();
				rez=o.divPol(p1, p2);
				rezText.setText(rez.get(0).toString());
				restText.setText(rez.get(1).toString());
			}	
		});
		derp1Btn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p1 = readPol(p1Text.getText());
				rezText.setText(o.derivare(p1).toString());
			}	
		});
		derp2Btn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p2 = readPol(p2Text.getText());
				rezText.setText(o.derivare(p2).toString());
			}	
		});
		intp1Btn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p1 = readPol(p1Text.getText());
				rezText.setText(o.integreaza(p1).toString());
			}	
		});
		intp2Btn.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				p2 = readPol(p2Text.getText());
				rezText.setText(o.integreaza(p2).toString());
			}	
		});
}
	

	public static void main(String[] args){
		new PolinomGui();
	}


}
