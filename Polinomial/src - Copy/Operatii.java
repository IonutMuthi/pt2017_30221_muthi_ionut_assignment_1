import java.util.ArrayList;

public class Operatii  {
	
	public Operatii(){
		
	}
	
	
	// functie de adunare a 2 polinaome
	public Polinom addPol(Polinom p1, Polinom p2){
		ArrayList<Monom> aux=new ArrayList();
		ArrayList<Monom> aux2=new ArrayList();
		Polinom paux=new Polinom(null);
		Polinom paux2=new Polinom(null);
		Polinom paux1=new Polinom(null);
		for(Monom m1:p1.getPol()){  
			for(Monom m2:p2.getPol()){
				if(m1.getGrad()==m2.getGrad()){ // daca 2 monoame au garadele egale atunci adunam coeficientii si copiem 
					Monom maux=new Monom((m1.getCoeficient()+m2.getCoeficient()),m1.getGrad());//gradul de la unul din ele
					paux.addM(maux);  //monomul rezultat este stocat intr-un polinom auxiliar	
					paux2.addM(m2);
					paux1.addM(m1);
				}		
			}
		}
		for(Monom m1:p1.getPol()){  // copiem monoamele ramase din primul polinom in polinomul auxiliar
			if(paux1.contenins(m1)==false)
				paux.addM(m1);	
		}
		for(Monom m1:p2.getPol()){  // copiem monoamele ramase din al doile polinom in polinomul auxiliar
			if(paux2.contenins(m1)==false)
				paux.addM(m1);
		}
		return paux;
	}
	

	
	// diferenta a doua polinoame
	public Polinom subPol(Polinom p1, Polinom p2){ // algoritmul este aceleasi ca la adunare doar ca caoeficientii se scad
		Polinom paux=new Polinom(null);
		for(Monom m1:p2.getPol()){
			Monom maux=new Monom(-m1.getCoeficient(),m1.getGrad());
		paux.addM(maux);
		}
		paux=addPol(p1,paux);
		Polinom paux2=new Polinom(null);
		paux2=addPol(paux2, paux);
		for(Monom m2:paux2.getPol()){
			if(m2.getCoeficient()==0)
				paux.removeM(m2);
		}
		return paux;
	}
	
	// derivarea polinoamelor 
	public Polinom derivare(Polinom p1){
		Polinom paux=new Polinom(new ArrayList());
		for(Monom m:p1.getPol()){
			Monom maux=new Monom((m.getCoeficient()*m.getGrad()),(m.getGrad()-1));
			paux.addM(maux);
		}
		return paux;
	}
	
	
	//integrarea polinoamelor
	public Polinom integreaza(Polinom p1){
		ArrayList<Monom> aux=new ArrayList();
		Polinom paux=new Polinom(null);
		for(Monom m:p1.getPol()){
			Monom maux=new Monom((m.getCoeficient()/(m.getGrad()+1)),(m.getGrad()+1)
					);
			aux.add(maux);
		}
		paux.setPol(aux);
		return paux;
	}
	
	
	// functie pentru a aduna monoamele de acelasi grad dintr-un polinom
	public Polinom addPol(Polinom p1){
		Polinom paux=new Polinom(null);
		Polinom paux2=new Polinom(null);
	//	Polinom paux3=new Polinom(null);
		for(Monom m1:p1.getPol()){
			for(Monom m2:p1.getPol()){ 
				Monom maux=new Monom(0,0);
				if(p1.getIndex(m1)>p1.getIndex(m2)){ // ne asiguram ca nu aduman acelasi termen de mai multe ori
				if(m1.getGrad()==m2.getGrad()){ // dupa care aplicam principiul de la adunarea a doua polinoame 
					maux.setCoeficient(m1.getCoeficient()+m2.getCoeficient());
					maux.setGrad(m1.getGrad());
					paux.addM(maux);
					paux2.addM(m1);
				    }
				}
				
			}			
		}	
		for(Monom m1:p1.getPol()){ // termenii ramasi se copiaza in polinomul auxiliar
			if(paux2.contenins(m1)==false)
				paux.addM(m1);
		}

		return paux;
	}
	
	//inmultirea polinoamelor
	public Polinom mulPol(Polinom p1,Polinom p2){
		Polinom paux=new Polinom(null);
		
		for(Monom m1:p1.getPol()){
			for(Monom m2:p2.getPol()){ //se inmultesc toate monoamele din primul cu toate din polinomul doi is se aduna gradele
				Monom maux=new Monom(0,0);
				maux.setCoeficient(m1.coeficient*m2.coeficient);
				maux.setGrad(m1.grad+m2.grad);
				paux.addM(maux);
			}
		}
		paux=addPol(paux); // adunam monoamele de acelasi grad
		return paux;
	}
	
	
	// inmultirea unui polinom cu un monom area celasi principiu ca inmultirea a 2 polinoame
	public Polinom mulMo(Polinom p1,Monom m1){
		Polinom paux= new Polinom(null);
		
		for(Monom m:p1.getPol()){
			Monom maux=new Monom(0,0);
			maux.setCoeficient(m.getCoeficient()*m1.getCoeficient());
			maux.setGrad(m.getGrad()+m1.getGrad());
			paux.addM(maux);
		}
		return paux;
	}
	
	//impartirea a 2 polinoame
	
	public ArrayList<Polinom> divPol(Polinom p1, Polinom p2){
		ArrayList<Polinom> rez=new ArrayList();
		Polinom cat=new Polinom(null);
		Polinom d=new Polinom(null);
		d=p1;	
		while(d.getMax().getGrad()>=p2.getMax().getGrad()){	
			Polinom paux=new Polinom(null);	
			Monom maux=new Monom(0,0);
			maux=maux.impMonom(d.getMax(),p2.getMax());
			cat.addM(maux);
			paux=mulMo(p2,maux);
			d=subPol(d,paux);
		
		}
		
		rez.add(cat);
		rez.add(d);
		return rez;	
	}
	
}
