import java.util.ArrayList;

public class Polinom  {

	ArrayList<Monom> pol=new ArrayList();
	
	public Polinom(ArrayList<Monom> pol){
		this.pol=new ArrayList();
	}
	
	public ArrayList<Monom> getPol() {
		return pol;
	}
	
	public void setPol(ArrayList<Monom> pol){
		this.pol=pol;
	}
	
	public Monom getMax(){
	int max=0;
	Monom m1=new Monom(0,0);
		for(Monom m:pol)
		{
			if(m.getGrad()>max){
				max=m.getGrad();
				m1.setCoeficient(m.getCoeficient());
				m1.setGrad(max);
			}
			}
		return m1;
	}
	
	public void addM(Monom m){
		pol.add(m);
	}

	public void removeM(Monom m){
		pol.remove(m);
	}
	
	public int getIndex(Monom m){
		
		return pol.indexOf(m);
		
	}
	
	public boolean contenins(Monom m){
		return pol.contains(m);
	}
	
	
	public void afis(){
		for(Monom m: pol){
			System.out.print(String.format( "+" +"%.1f",m.getCoeficient()) + "x^" + m.getGrad() );
		}
	}
	
	public Monom plusMonom(String input){
		Monom m=new Monom(0,0);
		if(input.contains("x")){
			for(int i=0;i<input.length();i++){
				if(input.substring(i,i+1).equals("x")){
				String str1=input.substring(2, i);
				if(str1.isEmpty())
					m.setCoeficient(0);
				else
					m.setCoeficient(Double.parseDouble(str1));
					m.setGrad(1);
			}
			else if(input.substring(i,i+1).equals("^")){
				String str2=input.substring(i+1, input.length());
				str2=str2.replaceAll("\\s", "");
				if(str2.isEmpty())
					m.setGrad(0);
				else
					m.setGrad(Integer.parseInt(str2));
			}
			}
		}
		else{
			String str3=input.substring(2, input.length());
			m.setCoeficient(Double.parseDouble(str3));
			m.setGrad(0);
		}
		return m;
	}
	
	public Monom minusMonom(String input){
		Monom m=new Monom(0,0);
		if(input.contains("x")){
			for(int i=0;i<input.length();i++){
				if(input.substring(i,i+1).equals("x")){
				String str1=input.substring(2, i);
				if(str1.isEmpty())
					m.setCoeficient(0);
				else
					m.setCoeficient(0-Double.parseDouble(str1));
					m.setGrad(1);
			}
			else if(input.substring(i,i+1).equals("^")){
				String str2=input.substring(i+1, input.length());
				str2=str2.replaceAll("\\s", "");
				if(str2.isEmpty())
					m.setGrad(0);
				else
					m.setGrad(Integer.parseInt(str2));
			}
			}
		}
		else{
			String str3=input.substring(2, input.length());
			m.setCoeficient(0-Double.parseDouble(str3));
			m.setGrad(0);
		}
		return m;
	}
	
	public Polinom citire(String input){
		Polinom p=new Polinom(null);
		ArrayList<Monom> pol=new ArrayList();
		input=input.replaceAll("\\s","");
		input=input.replaceAll("\\+"," + ");
		input=input.replaceAll("\\-"," - " );
		String str=new String(input);
		for(String ret:str.split("(?=\\+)|(?=\\-)")){
			for(int i=0;i<ret.length();i++){
				if(ret.substring(i, i+1).equals("+"))
				if(!(plusMonom(ret).getCoeficient()==0)){
					pol.add(plusMonom(ret));
				}
				if(ret.substring(i, i+1).equals("-"))
					if(!(minusMonom(ret).getCoeficient()==0)){
						pol.add(minusMonom(ret));
					}
					
			}
		}
		p.setPol(pol);
		return p;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Polinom other = (Polinom) obj;
		if (pol == null) {
			if (other.pol != null)
				return false;
		} else if (!pol.equals(other.pol))
			return false;
		return true;
	}

	@Override
	public String toString(){
		String rezPol= "";
		for(Monom m:pol){
			rezPol +=m.toString()+" ";
		}
		return rezPol;
	}
	
}
